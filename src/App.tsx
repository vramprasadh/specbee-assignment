import PageLayout from 'components/PageLayout/PageLayout';

import './App.css';

function App() {
  return (
    <div>
      <PageLayout />
    </div>
  );
}

export default App;
