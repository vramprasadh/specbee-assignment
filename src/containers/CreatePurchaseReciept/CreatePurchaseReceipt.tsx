import { FC, Fragment } from "react";

import { Box, Flex, Heading, Stack } from "@chakra-ui/layout";
import { Button } from "@chakra-ui/button";
import { useColorModeValue } from "@chakra-ui/color-mode";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { Menu, MenuButton, MenuList, MenuItem } from "@chakra-ui/menu";
import { useBreakpointValue } from "@chakra-ui/media-query";
import CreatePurchaseForm from "components/CreatPurchaseForm/CreatePurchaseForm";

export const ActionsMenu = () => {
  return (
    <Menu colorScheme="blue">
      <MenuButton as={Button} rightIcon={<ChevronDownIcon />}>
        Actions
      </MenuButton>
      <MenuList>
        <MenuItem>New</MenuItem>
        <MenuItem>View</MenuItem>
        <MenuItem>Print</MenuItem>
        <MenuItem>Delete</MenuItem>
        <MenuItem>Save as Draft</MenuItem>
        <MenuItem>Submit</MenuItem>
      </MenuList>
    </Menu>
  );
};

const CreatePurchaseReceipt: FC = () => {
  const actionButtonHideOrShow = useBreakpointValue({
    base: "none",
    lg: "flex",
  });
  return (
    <Fragment>
      <Box
        px="4"
        position="fixed"
        w="100%"
        boxShadow="md"
        bgColor={useColorModeValue("gray.200", "gray.700")}
        zIndex="sticky"
      >
        <Flex
          h={16}
          alignItems={"center"}
          justifyContent={"space-between"}
        >
          <Heading as="h3" fontSize={["sm", "lg"]}>
            Create a purchase receipt
          </Heading>
          <Flex alignItems="center">
            <Stack direction="row" spacing="7" display={actionButtonHideOrShow}>
              <Button size="sm" colorScheme="blue" variant="outline">
                New
              </Button>
              <Button size="sm" colorScheme="blue" variant="outline">
                View
              </Button>
              <Button size="sm" colorScheme="blue" variant="outline">
                Print
              </Button>
              <Button size="sm" colorScheme="blue" variant="outline">
                Delete
              </Button>
              <Button size="sm" colorScheme="blue" variant="outline">
                Save as Draft
              </Button>
              <Button size="sm" colorScheme="blue" variant="solid">
                Submit
              </Button>
            </Stack>
            {actionButtonHideOrShow === "none" && <ActionsMenu />}
          </Flex>
        </Flex>
      </Box>
      <Box marginTop="20" px="7">
        <Heading as="h4" fontWeight="semibold" fontSize="md" borderBottom="1px" borderBottomColor="gray.500" py="2">Supplier Details</Heading>
        <Box p={["2", "20"]}>
            <CreatePurchaseForm />
        </Box>
      </Box>
    </Fragment>
  );
};

export default CreatePurchaseReceipt;
