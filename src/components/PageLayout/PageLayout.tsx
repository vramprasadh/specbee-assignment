import { FC, Fragment } from "react";

import NavigationBar from "components/NavigationBar/NavigationBar";
import Main from "components/Main/Main";

const PageLayout: FC = () => {
  return (
    <Fragment>
        <NavigationBar />
        <Main />
    </Fragment>
  );
};

export default PageLayout;