import { FC } from "react";

import { Box } from "@chakra-ui/layout";

import CreatePurchaseReceipt from "containers/CreatePurchaseReciept/CreatePurchaseReceipt";

const Main: FC = ({ children }) => {
  return (
    <Box paddingTop="64px" minH={"55rem"} width="100%">
      <CreatePurchaseReceipt />
    </Box>
  );
};

export default Main;
