import React, { FC } from "react";

import { Box, Center, Flex, Heading, Stack } from "@chakra-ui/layout";
import { IconButton } from "@chakra-ui/button";
import { useColorModeValue } from "@chakra-ui/color-mode";
import { MoonIcon, SunIcon, BellIcon, Search2Icon } from "@chakra-ui/icons";
import {
  Avatar,
  AvatarBadge,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  useColorMode,
  useBreakpointValue,
} from "@chakra-ui/react";

const NavigationBar: FC = () => {
  //   const { isOpen, onOpen, onClose } = useDisclosure();
  const { colorMode, toggleColorMode } = useColorMode();
  const mobileDisplay = useBreakpointValue({ base: "flex", md: "none" });
  const desktopDisplay = useBreakpointValue({ base: "none", md: "flex" });

  return (
    <Box
      bgColor={useColorModeValue("gray.300", "gray.900")}
      px={4}
      position="fixed"
      w="100%"
      zIndex="popover"
    >
      <Flex h={16} alignItems={"center"} justifyContent={"space-between"}>
        <Box>
          <Heading>Livine</Heading>
        </Box>
        <Flex alignItems="center">
          <Stack direction="row" spacing="7">
            <IconButton
              bg="transparent"
              rounded="3xl"
              aria-label={"Search"}
              size="lg"
              icon={<Search2Icon />}
            />
            <IconButton
              display={desktopDisplay}
              bg="transparent"
              rounded="3xl"
              aria-label={"Search"}
              size="lg"
              icon={<BellIcon />}
            />
            <IconButton
              display={desktopDisplay}
              onClick={toggleColorMode}
              bg="transparent"
              rounded="3xl"
              size="lg"
              aria-label={
                colorMode === "light" ? "Switch to Dark" : "Switch to Light"
              }
              icon={colorMode === "light" ? <MoonIcon /> : <SunIcon />}
            />
            <Menu>
              <MenuButton as="button" rounded="full" cursor="pointer">
                <Avatar
                  size={"sm"}
                  src={
                    "https://images.unsplash.com/photo-1493666438817-866a91353ca9?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=b616b2c5b373a80ffc9636ba24f7a4a9"
                  }
                >
                  <AvatarBadge boxSize="1.25em" bg="green.500" />
                </Avatar>
              </MenuButton>
              <MenuList alignItems={"center"}>
                <br />
                <Center>
                  <Avatar
                    size={"2xl"}
                    src={
                      "https://images.unsplash.com/photo-1493666438817-866a91353ca9?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=b616b2c5b373a80ffc9636ba24f7a4a9"
                    }
                  />
                </Center>
                <br />
                <Center>
                  <p>Jane Doe</p>
                </Center>
                <Center display={mobileDisplay}>
                  <IconButton
                    bg="transparent"
                    rounded="3xl"
                    aria-label={"Search"}
                    size="lg"
                    icon={<BellIcon />}
                  />
                  <IconButton
                    onClick={toggleColorMode}
                    bg="transparent"
                    rounded="3xl"
                    size="lg"
                    aria-label={
                      colorMode === "light"
                        ? "Switch to Dark"
                        : "Switch to Light"
                    }
                    icon={colorMode === "light" ? <MoonIcon /> : <SunIcon />}
                  />
                </Center>
                <br />
                <MenuDivider />
                <MenuItem>Account Settings</MenuItem>
                <MenuItem>Logout</MenuItem>
              </MenuList>
            </Menu>
          </Stack>
        </Flex>
      </Flex>
    </Box>
  );
};

export default NavigationBar;
