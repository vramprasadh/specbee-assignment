import { FC, useState } from "react";

import { Box, Stack } from "@chakra-ui/layout";
import {
  FormControl,
  FormLabel,
  Input,
  Select,
  InputRightElement,
  InputGroup,
} from "@chakra-ui/react";
import DatePicker, { DayValue } from "react-modern-calendar-datepicker";

import "react-modern-calendar-datepicker/lib/DatePicker.css";
import { CalendarIcon } from "@chakra-ui/icons";

const CreatePurchaseForm: FC = () => {
  const [selectedDay, setSelectedDay] = useState<DayValue>(null);
  const [selectedTransactionDate, setSelectedTransactionDate] =
    useState<DayValue>(null);
  const [selectedInvoiceDate, setSelectedInvoiceDate] =
    useState<DayValue>(null);
  const renderCustomInput = ({ ref }: any) => {
    return (
      <Input
        className="my-custom-input-class"
        readOnly
        ref={ref}
        placeholder="Quote Date"
        value={
          selectedDay
            ? `${selectedDay.day}/${selectedDay.month}/${selectedDay.year}`
            : ""
        }
        width={["288px", "", "264px", "392px", "770px"]}
      />
    );
  };
  const renderSelectedTransactionDate = ({ ref }: any) => {
    return (
      <Input
        className="my-custom-input-class"
        readOnly
        ref={ref}
        placeholder="Transaction Date"
        value={
          selectedTransactionDate
            ? `${selectedTransactionDate.day}/${selectedTransactionDate.month}/${selectedTransactionDate.year}`
            : ""
        }
        width={["288px", "", "264px", "392px", "770px"]}
      />
    );
  };
  const renderSelectedInvoiceDate = ({ ref }: any) => {
    return (
      <Input
        className="my-custom-input-class"
        readOnly
        ref={ref}
        placeholder="Invoice Date"
        value={
          selectedInvoiceDate
            ? `${selectedInvoiceDate.day}/${selectedInvoiceDate.month}/${selectedInvoiceDate.year}`
            : ""
        }
        width={["288px", "", "264px", "392px", "770px"]}
      />
    );
  };
  return (
    <Stack direction={["column", "column", "row"]} zIndex="base">
      <Box w={["72", "72", "72", "3xl"]} mr={["0", "4"]}>
        <FormControl mb="2.5" id="supplier-name" isRequired>
          <FormLabel>Supplier Name</FormLabel>
          <Input placeholder="Supplier Name" />
        </FormControl>
        <FormControl mb="2.5" id="unit-warehouse" isRequired>
          <FormLabel>Unit/Warehouse</FormLabel>
          {/* <Input placeholder="Unit/WareHouse" /> */}
          <Select placeholder="Unit/Warehouse">
            <option>Bangalore</option>
            <option>Salem</option>
            <option>Chennai</option>
            <option>Kochi</option>
            <option>Mumbai</option>
          </Select>
        </FormControl>
        <FormControl mb="2.5" id="quote-number">
          <FormLabel>Quote Number</FormLabel>
          <Input placeholder="Quote Number" />
        </FormControl>
        <FormControl mb="2.5" id="invoice-number">
          <FormLabel>Invoice Number</FormLabel>
          <Input placeholder="Invoice Number" />
        </FormControl>
        <FormControl mb="2.5" id="delivery-note">
          <FormLabel>Deliver Note</FormLabel>
          <Input placeholder="Deliver Note" />
        </FormControl>
        <FormControl mb="2.5" id="quote-date">
          <FormLabel>Quote Date</FormLabel>
          <InputGroup>
            {/* <Input placeholder="First name" /> */}
            <DatePicker
              value={selectedDay}
              onChange={(date) => setSelectedDay(date)}
              renderInput={renderCustomInput}
              shouldHighlightWeekends
            />
            <InputRightElement children={<CalendarIcon color="gray" />} />
          </InputGroup>
        </FormControl>
      </Box>
      <Box w={["72", "72", "72", "3xl"]}>
        <FormControl mb="2.5" id="supplier-address" isRequired>
          <FormLabel>Supplier Address</FormLabel>
          <Input placeholder="Supplier Address" />
        </FormControl>
        <FormControl mb="2.5" id="transaction-date" isRequired>
          <FormLabel>Transaction Date</FormLabel>
          {/* <Input placeholder="Transaction Date" /> */}
          <InputGroup>
            <DatePicker
              value={selectedTransactionDate}
              onChange={(date) => setSelectedTransactionDate(date)}
              renderInput={renderSelectedTransactionDate}
              shouldHighlightWeekends
            />
            <InputRightElement children={<CalendarIcon color="gray" />} />
          </InputGroup>
        </FormControl>
        <FormControl mb="2.5" id="unit-warehouse">
          <FormLabel>Unit/Warehouse</FormLabel>
          {/* <Input placeholder="Unit/WareHouse" /> */}
          <Select placeholder="Price List Template">
            <option>Rupee</option>
            <option>Dollar</option>
            <option>WON</option>
          </Select>
        </FormControl>
        <FormControl mb="2.5" id="transaction-date">
          <FormLabel>Invoice Date</FormLabel>
          {/* <Input placeholder="Transaction Date" /> */}
          <InputGroup>
            <DatePicker
              value={selectedTransactionDate}
              onChange={(date) => setSelectedInvoiceDate(date)}
              renderInput={renderSelectedInvoiceDate}
              shouldHighlightWeekends
            />
            <InputRightElement children={<CalendarIcon color="gray" />} />
          </InputGroup>
        </FormControl>
        <FormControl mb="2.5" id="consignment-agent">
          <FormLabel>Consignment Agent</FormLabel>
          {/* <Input placeholder="Unit/WareHouse" /> */}
          <Select placeholder="Consignment Agent">
            <option>Blue Dart</option>
            <option>Professional Couriers</option>
            <option>Express Bee</option>
          </Select>
        </FormControl>
        <Stack direction={["column", "row"]}>
          <FormControl mb="2.5" id="item-code">
            <FormLabel>Item Code</FormLabel>
            <Input placeholder="Item Code" />
          </FormControl>
          <FormControl mb="2.5" id="price">
            <FormLabel>Price</FormLabel>
            <Input placeholder="Price" />
          </FormControl>
        </Stack>
      </Box>
    </Stack>
  );
};

export default CreatePurchaseForm;
